<?php

namespace App\Service\Mailing;

use App\Service\MessageGenerator;
use Psr\Log\LoggerInterface;

class Sender
{
    private $logger;

    private $generator;

    /**
     * Sender constructor.
     * @param LoggerInterface $logger
     * @param MessageGenerator $generator
     */
    public function __construct(LoggerInterface $logger, MessageGenerator $generator)
    {
        $this->logger = $logger;
        $this->generator = $generator;
    }

    /**
     * @param string $code
     * @param string $pathToAttachment
     */
    public function send(string $code, string $pathToAttachment)
    {
        $this->process($code, $pathToAttachment);
        $this->sendOut();
    }

    /**
     * @param string $code
     * @param string $pathToAttachment
     */
    private function process(string $code, string $pathToAttachment)
    {
        $this->generator->setCode($code);
        $this->generator->setAttachment($pathToAttachment);
        $this->generator->create();
    }

    private function sendOut()
    {
        // some code for sending email using swiftmailer
    }
}