<?php

namespace App\Service\Mailing;

use App\Repository\Mailing\MailingDataRepository;
use App\Service\MessageGenerator;

class MailCreator implements MessageGenerator
{
    private $code;

    private $pathToAttachment;

    private $mailingRepository;

    /**
     * MailCreator constructor.
     * @param MailingDataRepository $mailingRepository
     */
    public function __construct(MailingDataRepository $mailingRepository)
    {
        $this->mailingRepository = $mailingRepository;
    }

    public function create(): void
    {
        // TODO: Implement create() method.
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode(string $code = ""): void
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getPathToAttachment()
    {
        return $this->pathToAttachment;
    }

    /**
     * @param mixed $pathToAttachment
     */
    public function setAttachment($pathToAttachment = ""): void
    {
        $this->pathToAttachment = $pathToAttachment;
    }
}