<?php

namespace App\Service;

interface MessageGenerator
{
    public function create(): void;

    /**
     * @param string $code
     * @return mixed
     */
    public function setCode(string $code = ""): void;

    /**
     * @param string $pathToAttachment
     * @return mixed
     */
    public function setAttachment(string $pathToAttachment = ""): void;
}