<?php

namespace App\Controller;

use App\Service\Mailing\Sender;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Flex\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SenderController
 * @package App\Controller
 *
 * @Route("/")
 */
class SenderController extends AbstractController
{
    /**
     * @var Sender
     */
    private $sender;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * SenderController constructor.
     * @param Sender $sender
     * @param LoggerInterface $logger
     */
    public function __construct(Sender $sender, LoggerInterface $logger)
    {
        $this->sender = $sender;
        $this->logger = $logger;
    }

    /**
     * @Route("/send", methods={"GET"}, name="send")
     */
    public function send(Request $request): Response
    {
        $code = $request->query->get('code');
        $pathToAttachment = $request->query->get('path_to_file');
        $this->sender->send($code, $pathToAttachment);
    }
}